﻿using System;
using System.Collections.Generic;
using System.Text;
using Challenge.MarsRover.Repos;
using Challenge.MarsRover.Models;
using System.Linq;

namespace Challenge.MarsRoverTests.Controllers
{
    public class RoverRepoFake : IRoverRepo
    {
        private List<Rover> _rovers;
        private int IDIndex;

        public RoverRepoFake()
        {
            _rovers = new List<Rover>()
            {
                new Rover(){RoverID=1,PublicRoverID="Test111",CurrentPosition=new Position(){PositionID=0,Direction=0,x=0,y=0 } },
                new Rover(){RoverID=2,PublicRoverID="Test222",CurrentPosition=new Position(){PositionID=0,Direction=0,x=2,y=3 } },
                new Rover(){RoverID=3,PublicRoverID="Test333",CurrentPosition=new Position(){PositionID=0,Direction=0,x=14,y=-4 } },
             //  new Rover(){RoverID=0,PublicRoverID="Test444",CurrentPosition=new Position(){PositionID=0,Direction=0,x=0,y=0 } },
             //  new Rover(){RoverID=0,PublicRoverID="Test555",CurrentPosition=new Position(){PositionID=0,Direction=0,x=0,y=0 } },




            };

            IDIndex = 3;
        }




        public Rover createRover(Rover R)
        {
            IDIndex++;
            R.RoverID = IDIndex;
            _rovers.Add(R);

            return _rovers[IDIndex-1];
        }

        public Rover getByID(int id)
        {
            return _rovers.Where(a => a.RoverID == id).FirstOrDefault();
        }

        public Rover getByPubID(string id)
        {
           // var rov= _rovers.Find(a => a.PublicRoverID == id);
            var rov = _rovers.Where(a => a.PublicRoverID == id).FirstOrDefault();
       

            return rov;
        }

        public List<Rover> getRovers()
        {
            return _rovers;
        }

        public void removeRover(Rover R)
        {
            throw new NotImplementedException();
        }

        public void updateRover(Rover R)
        {
            int tempindex = _rovers.FindIndex(a => a.RoverID == R.RoverID);
            _rovers[tempindex]=R;
           
        }
    }
}
