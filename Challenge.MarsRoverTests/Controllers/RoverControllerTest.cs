﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Challenge.MarsRover.Controllers;
using Challenge.MarsRover.Repos;
using Challenge.MarsRover.Models;
using Challenge.MarsRover.Services;
using Challenge.MarsRoverTests.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Challenge.MarsRoverTests
{
    public class RoverControllerTest
    {
        //integration tests
        
        IRoverRepo _roverRepo;
        RoverController _roverController;
        MovementService _movementService;

        public RoverControllerTest()
        {
            _roverRepo = new RoverRepoFake();
            _movementService = new MovementService();
            _roverController = new RoverController(_roverRepo, _movementService);

        }


        [Fact]
        public void Get_WithGoodID_WhenCalled_ReturnsOKResult()
        {
            //arrange

            //action
            var okres = _roverController.Get(1);

            //assert
            Assert.IsType<OkObjectResult>(okres.Result);
        
        }
        [Fact]
        public void Get_WithBadID_WhenCalled_ReturnsNotFound()
        {
            //arrange

            //action
            var okres = _roverController.Get(100);

            //assert
            Assert.IsType<NotFoundResult>(okres.Result);

        }

        [Fact]
        public void GetAllRovers_WhenCalled_ReturnsOKResult()
        {
            //action
            var okres = _roverController.GetAllRovers();

            //assert
            Assert.IsType<OkObjectResult>(okres.Result);
        }


        [Theory]
        [InlineData("Test111", "(0,0)")]
        [InlineData("Test222", "(2,3)")]
        [InlineData("Test333", "(14,-4)")]
        public void GetRover_WithGoodPublicID_WhenCalled_ReturnsOKResult_ReturnsPos(string roverPubID,string expected)
        {
            //action
            var actionresult = _roverController.GetRover(roverPubID);
            var okobjres = actionresult as OkObjectResult;
            var response = okobjres.Value as StandardResponse;


            //assert
            Assert.IsType<OkObjectResult>(actionresult);
            Assert.Equal(expected, response.CurrentPosition);

        }
        [Theory]
        [InlineData("Test111", "RMMLM", "(2,1)")]
        [InlineData("Test222", "RRRRRRRRRLLRRLLLLLRLRLLLLLLLLLLLRMMM", "(-1,3)")]
        [InlineData("Test333", "MMMRRMMMRRMMLLMMLM", "(15,-4)")]
        public void Move_WithGoodPublicID_WhenCalled_ReturnsOKResult_ReturnsPos(string roverPubID,string instructions, string expected)
        {
            //arrange
            var request = new MoveRequest() { MovementInstruction = instructions, RoverID = roverPubID };

            //action
            var actionresult = _roverController.Move(request);
            var okobjres = actionresult as OkObjectResult;
            var response = okobjres.Value as StandardResponse;


            //assert
            Assert.IsType<OkObjectResult>(actionresult);
            Assert.Equal(expected, response.CurrentPosition);

        }


        [Fact]
        public void Move_WithnoPubID_WhenCalled_ReturnsCreated_ReturnsPos()
        {
            //arrange
            var request = new MoveRequest() { MovementInstruction = "LMLM", RoverID = "" };
            string expected = "(-1,-1)";

            //action
            var actionresult = _roverController.Move(request);
            var okobjres = actionresult as OkObjectResult;
            var response = okobjres.Value as StandardResponse;

            bool iscreated = false;

            if (response.Message.Contains("Rover Created"))
                iscreated = true;

            //assert
            Assert.IsType<OkObjectResult>(actionresult);
            Assert.True(iscreated);
            Assert.Equal(expected, response.CurrentPosition);

        }


    }
}
