﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Challenge.MarsRover.Enums;
using Challenge.MarsRover.Models;

namespace Challenge.MarsRover.Services
{
    public class MovementService
    {
        Rover _rover;
        DirectionEnum direction;
        Position currentPos;
     


        

        public Rover ProcessMoves(Rover rover,string movements)
        {
            _rover = rover;
            direction = rover.CurrentPosition.Direction;
            currentPos = rover.CurrentPosition;
            

            foreach (char c in movements)
            {
                if (c == 'R')
                {
                    RotateRight();
                }
                if (c == 'L')
                {
                    RotateLeft();
                }
                if (c == 'M')
                {
                    Move();
                }



            }

            currentPos.Direction = direction;

            _rover.CurrentPosition = currentPos;
           

            return _rover;
        }
        private void RotateLeft()
        {

            if (direction == DirectionEnum.North)
            {
                direction = DirectionEnum.West;
            }
            else
                direction--;

        }

        private void RotateRight()
        {
            if (direction == DirectionEnum.West)
            {
                direction = DirectionEnum.North;
            }
            else
                direction++;
        }


        private void Move()
        {
            if (direction == DirectionEnum.North)
            {
                currentPos.y++;
            }
            if (direction == DirectionEnum.East)
            {
                currentPos.x++;
            }
            if (direction == DirectionEnum.South)
            {
                currentPos.y--;
            }
            if (direction == DirectionEnum.West)
            {
                currentPos.x--;
            }



        }
      

    }
}
