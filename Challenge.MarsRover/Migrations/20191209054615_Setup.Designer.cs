﻿// <auto-generated />
using Challenge.MarsRover.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Challenge.MarsRover.Migrations
{
    [DbContext(typeof(AppDBcontext))]
    [Migration("20191209054615_Setup")]
    partial class Setup
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.3-servicing-35854")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Challenge.MarsRover.Models.Position", b =>
                {
                    b.Property<int>("PositionID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("Direction");

                    b.Property<int>("x");

                    b.Property<int>("y");

                    b.HasKey("PositionID");

                    b.ToTable("Positions");
                });

            modelBuilder.Entity("Challenge.MarsRover.Models.Rover", b =>
                {
                    b.Property<int>("RoverID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("PublicRoverID")
                        .HasMaxLength(10);

                    b.Property<int>("CurrentPositionPositionID");

                    b.HasKey("RoverID", "PublicRoverID");

                    b.HasAlternateKey("PublicRoverID", "RoverID");

                    b.HasIndex("CurrentPositionPositionID");

                    b.ToTable("Rovers");
                });

            modelBuilder.Entity("Challenge.MarsRover.Models.Rover", b =>
                {
                    b.HasOne("Challenge.MarsRover.Models.Position", "CurrentPosition")
                        .WithMany()
                        .HasForeignKey("CurrentPositionPositionID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
