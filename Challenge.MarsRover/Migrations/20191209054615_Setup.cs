﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Challenge.MarsRover.Migrations
{
    public partial class Setup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Positions",
                columns: table => new
                {
                    PositionID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Direction = table.Column<int>(nullable: false),
                    x = table.Column<int>(nullable: false),
                    y = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Positions", x => x.PositionID);
                });

            migrationBuilder.CreateTable(
                name: "Rovers",
                columns: table => new
                {
                    RoverID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PublicRoverID = table.Column<string>(maxLength: 10, nullable: false),
                    CurrentPositionPositionID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rovers", x => new { x.RoverID, x.PublicRoverID });
                    table.UniqueConstraint("AK_Rovers_PublicRoverID_RoverID", x => new { x.PublicRoverID, x.RoverID });
                    table.ForeignKey(
                        name: "FK_Rovers_Positions_CurrentPositionPositionID",
                        column: x => x.CurrentPositionPositionID,
                        principalTable: "Positions",
                        principalColumn: "PositionID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Rovers_CurrentPositionPositionID",
                table: "Rovers",
                column: "CurrentPositionPositionID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Rovers");

            migrationBuilder.DropTable(
                name: "Positions");
        }
    }
}
