﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Challenge.MarsRover.Enums;

namespace Challenge.MarsRover.Models
{
    public class Position
    {
        public int PositionID { get; set; }
        public DirectionEnum Direction { get; set; }
        public int x { get; set; }
        public int y { get; set; }
    }
}
