﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.MarsRover.Models
{
    public class MoveRequest
    {

        [StringLength(10, ErrorMessage = "ID can be no longer than 10 characters.")]
                [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string RoverID { get; set; }


        [StringLength(100, ErrorMessage = "Instructions line can't be more than 100 characters.")]
        [MinLength(1, ErrorMessage = "Instructions must be more than 0 characters.")]
        [RegularExpression("^[R,L,M]*$", ErrorMessage = "Only use R, L,or M!")]        
        public string MovementInstruction { get; set; }

    }
}
