﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.MarsRover.Models
{
    public class StandardResponse
    {
        [Display(Name = "Message")]
        public string Message { get; set; }
        [Display(Name = "CurrentPosition")]
        public string CurrentPosition { get; set; }
    }
}
