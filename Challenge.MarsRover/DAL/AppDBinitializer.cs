﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Challenge.MarsRover.DAL;
using Challenge.MarsRover.Models;
using Challenge.MarsRover.Enums;

namespace Challenge.MarsRover.DAL

{
    public class AppDBinitializer
    {
        public static void Seed(AppDBcontext context)
        {
            if (!context.Rovers.Any())
            {
                var curPos = new Position() { Direction = DirectionEnum.South, x = 2, y = 2 };
               
                var oriPos = new Position() { Direction=DirectionEnum.North,x = 0, y = 0 };



                context.AddRange(
                 new Rover
                 {
                    PublicRoverID="test123",
                     CurrentPosition = oriPos,
                 },
                               
                 new Rover     
                 {
                     PublicRoverID = "test234",
                     CurrentPosition = curPos,
                             
                 }
                    
                 
                 );

                context.SaveChanges();


            }
            context.SaveChanges();

        }

    }
    }
