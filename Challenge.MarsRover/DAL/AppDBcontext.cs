﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

using Challenge.MarsRover.Models;



namespace Challenge.MarsRover.DAL
{
    public class AppDBcontext : DbContext
    {

        public AppDBcontext(DbContextOptions<AppDBcontext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Rover>().HasKey(table => new {
                table.RoverID,
                table.PublicRoverID
            });
        }

        public DbSet<Rover> Rovers { get; set; }
        public DbSet<Position> Positions { get; set; }

                              
    }
}