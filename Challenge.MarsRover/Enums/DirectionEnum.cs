﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.MarsRover.Enums
{
    public enum DirectionEnum
    {
        [Display(Name = "North")]
        North,
        [Display(Name = "East")]
        East,
        [Display(Name = "South")]
        South,
        [Display(Name = "West")]
        West
    }
}
