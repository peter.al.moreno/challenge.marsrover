﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Challenge.MarsRover.Models;
using Challenge.MarsRover.DAL;
using Microsoft.EntityFrameworkCore;

namespace Challenge.MarsRover.Repos
{
    public class RoverRepo : IRoverRepo
    {
        private readonly AppDBcontext _appDBcontext;

        public RoverRepo(AppDBcontext appDBcontext)
        {
             _appDBcontext= appDBcontext;
        }


        public Rover createRover(Rover R)
        {
            _appDBcontext.Rovers.Add(R);
            _appDBcontext.SaveChanges();

            return _appDBcontext.Rovers.LastOrDefault();
        }



        public Rover getByID(int id)
        {
            return _appDBcontext.Rovers.Include(g => g.CurrentPosition).FirstOrDefault(p => p.RoverID == id);
        }

        public Rover getByPubID(string id)
        {
            return _appDBcontext.Rovers.Include(g => g.CurrentPosition).FirstOrDefault(p => p.PublicRoverID == id);
        }

        public List<Rover> getRovers()
        {
            return _appDBcontext.Rovers.Include(g=>g.CurrentPosition).ToList();
        }

        public void removeRover(Rover R)
        {
            throw new NotImplementedException();
        }

        public void updateRover(Rover R)
        {
           _appDBcontext.Rovers.Update(R);
            _appDBcontext.SaveChanges();
        }
    }
}
