﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Challenge.MarsRover.DAL;
using Challenge.MarsRover.Models;

namespace Challenge.MarsRover.Repos
{
    public class PositionRepo : IPositionRepo
    {

        private readonly AppDBcontext _appDBcontext;

        public PositionRepo(AppDBcontext appDBcontext)
        {
             _appDBcontext= appDBcontext;
        }

        public void createPosition(Position Pos)
        {
            throw new NotImplementedException();
        }

        public Position getByID(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Position> getPositions()
        {
            throw new NotImplementedException();
        }

        public void removePosition(Position Pos)
        {
            throw new NotImplementedException();
        }

        public void updatePosition(Position Pos)
        {
            throw new NotImplementedException();
        }
    }
}
