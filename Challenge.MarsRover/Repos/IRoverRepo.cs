﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Challenge.MarsRover.Models;

namespace Challenge.MarsRover.Repos
{
    public interface IRoverRepo
    {

        List<Rover> getRovers();

        Rover getByID(int id);
        Rover getByPubID(string id);

        Rover createRover(Rover R);
        void updateRover(Rover R);
        void removeRover(Rover R);

    }
}
