﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Challenge.MarsRover.Models;
namespace Challenge.MarsRover.Repos
{
    public interface IPositionRepo
    {
        IEnumerable<Position> getPositions();

        Position getByID(int id);

        void createPosition(Position Pos);
        void updatePosition(Position Pos);
        void removePosition(Position Pos);


    }
}
