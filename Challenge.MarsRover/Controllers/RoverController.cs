﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Challenge.MarsRover.Repos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Challenge.MarsRover.Models;
using System.Text.RegularExpressions;
using Challenge.MarsRover.Services;

namespace Challenge.MarsRover.Controllers
{
    [ApiVersion("1.0")]
    //[Route("api/v{version:apiVersion}/[controller]")]
    [Route("api/v1")]
    [ApiController]
    public class RoverController : ControllerBase
    {
        private readonly IRoverRepo _roverRepo;
        private readonly MovementService _movementService;
        public RoverController(IRoverRepo roverrepo, MovementService movementService)
        {
            _movementService = movementService;
            _roverRepo = roverrepo;
        }




        // GET: api/Rover
        [HttpGet]
        [Route("AllRovers")]
        public ActionResult<List<Rover>> GetAllRovers()
        {
            var roverz = _roverRepo.getRovers();

            if (roverz == null)
            {
                return NoContent();
            }

            return Ok(roverz);
        }

        // GET: api/Rover/5
        [HttpGet()]
        [Route("Rover")]
        public ActionResult<Rover> Get(int id)
        {

            var rover = _roverRepo.getByID(id) ;

            if (rover == null)
            {
                return NotFound();            
            }

            return Ok(rover);
        }




        public bool VerifyId(string Id)
        {
            if (Id.Length > 10)
            return false;

            Regex r = new Regex("^[a-zA-Z0-9]*$");
            if (!r.IsMatch(Id))
                return false;


                return true;
        }



        [HttpGet()]
        [Route("RoverPos")]
        public IActionResult GetRover(string RoverId)
        {

            bool valid = VerifyId(RoverId);

            if (!valid)
            {
                return BadRequest("Id Format is incorrect");
                
            }
            
            var rover = _roverRepo.getByPubID(RoverId);

            if (rover == null)
            {
                return NotFound("No Rover found for RoverId: "+RoverId);
            }



            StandardResponse response = new StandardResponse()
            {
                Message = "Rover Located",
                CurrentPosition = "(" + rover.CurrentPosition.x + "," + rover.CurrentPosition.y + ")"
            };

            return Ok(response);


        }


        public string RandoPubID()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[10];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

           return new String(stringChars);


        }


        // POST: api/Rover
        [HttpPost]
        [Route("Move")]
        public IActionResult Move([FromBody] MoveRequest moveRequest)
        {
           


            string message = "";

            if (ModelState.IsValid)
            {




                int x, y;

                message = "Validated Request. ";
                Rover rover;


                if (moveRequest.RoverID == "")
                {
                    //create new Rover

                   Position nuPos = new Position() {Direction=0,x=0,y=0 };
                Rover nuRover = new Rover() {CurrentPosition= nuPos };

                    /*
                    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                    var stringChars = new char[10];
                    var random = new Random();

                    for (int i = 0; i < stringChars.Length; i++)
                    {
                        stringChars[i] = chars[random.Next(chars.Length)];
                    }

                    var finalString = new String(stringChars);

                    // nuRover.PublicRoverID = finalString;
                    */
                    nuRover.PublicRoverID = RandoPubID();

                    rover =  _roverRepo.createRover(nuRover);
                    message += "Rover Created, ID: "+rover.PublicRoverID+". ";
                }
                else {
                    rover = _roverRepo.getByPubID(moveRequest.RoverID);

                    message += "Rover Located. ";

                }

                if (rover == null)
                {
                    return NotFound(rover);
                }


                x = rover.CurrentPosition.x;
                y = rover.CurrentPosition.y;


                var roverChange= _movementService.ProcessMoves(rover,moveRequest.MovementInstruction);
                _roverRepo.updateRover(roverChange);

                message += "Moved from " + "(" + x + "," + y + ")";


                StandardResponse response = new StandardResponse()
                {
                    Message = message,
                    CurrentPosition = "(" + roverChange.CurrentPosition.x + "," + roverChange.CurrentPosition.y + ")"
                };
                return Ok(response);


            }
            else
            {
                return BadRequest(moveRequest);
            }


            //return Ok(message);
        }

    }
}
